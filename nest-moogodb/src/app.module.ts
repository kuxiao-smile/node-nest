import { Module, MiddlewareConsumer, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserCopyController } from './user_copy/user_copy.controller';
import { UserCopyModule } from './user_copy/user_copy.module';
import { FileController } from './file/file.controller';
import { FileModule } from './file/file.module';

// moogodb 数据库 https://docs.nestjs.cn/9/techniques?id=mongo
import { MongooseModule } from '@nestjs/mongoose';

// 表
import { user, userSchema } from './user_copy/user_copy.outside'
import { file, fileSchema } from './file/file.outside'

// 全局存储数据方法
import { GlobalParamsService } from './utils/index'

// 跨域 
import * as cors from 'cors';

// npm i @nestjs/jwt
// 生成token https://docs.nestjs.cn/9/security?id=jwt-%e5%8a%9f%e8%83%bd
import { JwtModule } from '@nestjs/jwt';
import { toeknData, JwtExpiredMiddleware } from './utils/tokenConfig'

// 日志 - 配置包描述
// 日志库
import * as winston from 'winston';
// 日志库的插件
import { WinstonModule } from 'nest-winston';
// 一个用于 winston 日志库的插件 // https://www.npmjs.com/package/winston-daily-rotate-file
import * as DailyRotateFile from 'winston-daily-rotate-file';

//拦截器
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
// 全局响应拦截器
import { UnifyResponseInterceptor } from './unify-response.interceptor';
// 全局异常过滤器
import UnifyExceptionFilter from './uinify-exception.filter';
@Module({
  imports: [
    // 配置生成的token
    JwtModule.register({
      secret: toeknData.secret, // 是用于签名和验证 JWT 的密钥
      signOptions: { expiresIn: toeknData.expiresIn }, // JWT 的过期时间。
    }),
    // 连接moogodb数据库
    MongooseModule.forRoot('mongodb://localhost/nest'),
    // 挂载user表 来源：./user_copy/user_copy.outside
    MongooseModule.forFeature([{ name: user.name, schema: userSchema }]),
    // 挂载file公共服务表 来源：./file/file.outside
    MongooseModule.forFeature([{ name: file.name, schema: fileSchema }]),
    // 日志配置
    WinstonModule.forRoot({
      transports: [
        new DailyRotateFile({
          dirname: `logs`, // 日志保存的目录
          // dirname: `D:\\`, // 日志保存的目录 // 保存到本地
          filename: '%DATE%.log', // 日志名称，%DATE% 占位符表示日期。。
          datePattern: 'YYYY-MM-DD', // 日志轮换的频率，此处表示每天。
          zippedArchive: true, // 是否通过压缩的方式归档被轮换的日志文件。
          maxSize: '20m', // 设置日志文件的最大大小，m 表示 mb 。
          maxFiles: '14d', // 保留日志文件的最大天数，此处表示自动删除超过 14 天的日志文件。
          // 记录时添加时间戳信息
          format: winston.format.combine(
            winston.format.timestamp({
              format: 'YYYY-MM-DD HH:mm:ss',
            }),
            winston.format.json(),
          ),
        }),
      ],
    }),
    UserCopyModule,
    FileModule,
  ],
  controllers: [
    AppController,
    UserCopyController, 
    FileController,
  ],
  providers: [
    AppService,
    GlobalParamsService,
    // 应用响应拦截器
    {
      provide: APP_INTERCEPTOR,
      useClass: UnifyResponseInterceptor,
    },
    // 应用全局异常过滤器
    {
      provide: APP_FILTER,  
      useClass: UnifyExceptionFilter,
    },
  ],
})
// export class AppModule {}
// 定义中间件
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      // 解决跨域的问题的全局中间件
      .apply(cors()).forRoutes('*')
      // 验证token的全局中间件
      .apply(JwtExpiredMiddleware).forRoutes('*')
  }
}