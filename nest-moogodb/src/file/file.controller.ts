import { Controller, UseInterceptors, UploadedFile, Get, Post, Res, Req, Body, Query } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
// 文件上传  https://docs.nestjs.cn/9/techniques?id=%e6%96%87%e4%bb%b6%e4%b8%8a%e4%bc%a0
import { FileInterceptor } from '@nestjs/platform-express'
import { Express, Response, Request } from 'express'
// 生成唯一标识符
import * as nuid from 'nuid';
import * as fs from 'fs';
// 返回文件流 https://docs.nestjs.cn/9/techniques?id=%e6%b5%81%e5%a4%84%e7%90%86%e6%96%87%e4%bb%b6
import { createReadStream } from 'fs';
import { join } from 'path';

import { ApiTags, ApiQuery, ApiBody, ApiConsumes, ApiOperation, ApiCreatedResponse } from '@nestjs/swagger';
import { file } from './file.outside';

// 解析文件扩展名
const mime = require('mime-types');

@Controller('file')
export class FileController {
  constructor(
    // 依赖注入表文件，别名fileModel 此表已挂载到app.module.ts文件内
    @InjectModel('file') private readonly fileModel,
  ) { }

  // 文件上传
  @Post('upload') // 请求路径 /file/upload
  // Swagger标签
  @ApiTags('公共文件服务')
  // 文档声明请求参数格式类型
  @ApiConsumes('multipart/form-data')
  // 设置文档请求体
  @ApiBody({
    description: '上传文件',
    type: 'multipart/form-data',
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })

  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file, @Req() req: Request, @Body() body) {
    console.log(file.mimetype);
    
    // 设置文件的唯一文件名
    const uniqueSuffix = `${nuid.next()}.${file.mimetype.split('/')[1]}`;
    const filePath = './uploads/' + uniqueSuffix;
    // 存储文件
    fs.writeFileSync(filePath, file.buffer);

    const params = {
      id_data: body.id_data ? body.id_data : null,
      fileName: uniqueSuffix,
      fileOriginalName: file.originalname
    }

    // create 插入一条数据，直接将接收到的body参数插入
    const data = await this.fileModel.create(params)
    return { code: 200, data: data, message: "上传成功" } 

  }

  // 返回文件
  @Get('blobFile') // 请求路径 /file/blobFile
  // Swagger标签
  @ApiTags('公共文件服务')
  @ApiQuery({ name: 'file' })
  async getFile(@Res() res: Response, @Query() query) {
    // 获取文件真实文件名
    const data = await this.fileModel.find({ fileName: query.fileName })
    if (data && data.length) {
      // 获取文件位置
      const filePath = join(process.cwd(), `uploads/${query.fileName}`);
      // 创建可读流
      const stream = createReadStream(filePath);
      // 解析文件类型
      const mimeType = mime.lookup(filePath) || 'application/octet-stream';
      // 声明类型
      res.setHeader('Content-Type', 'application/octet-stream');
      res.setHeader('Content-Type', mimeType);
      // 设置下载的文件名
      res.setHeader('Content-Disposition', `attachment; filename="${encodeURIComponent(`${data[0].fileOriginalName}`)}"`);
      // 将读取的结果以管道pipe流的方式返回
      stream.pipe(res);
    } else {
      res.send({
        status: 201,
        data: [],
        message: '不存在该文件！'
      })
    }
  }
}
