import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { dateFormat } from '../utils'
// 表文件
export type CatDocument = file & Document;

@Schema()
// 定义了一个名为user的类，该类继承自Document类，表示该类的实例可以被存储到数据库中。
export class file extends Document {

  // @Prop()装饰器可以接受一些参数，例如default，用于指定属性的默认值。

  // 文件是由文件名查询后返回

  @Prop() // 如果有数据跟文件存在关联关系则需传id_data  值为数据的_id   如果不传则视为公共文件
  id_data:string

  @Prop({default: ''}) // 服务端文件名 默认 ''
  fileName: string;

  @Prop({default: ''}) // 上传文件名 默认 ''
  fileOriginalName: string;
  
  // default 如果不传则添加默认值
  @Prop({default: dateFormat('data')}) // 创建时间 默认当前时间
  date: string;
}

// 创建了一个名为userSchema的模式，该模式基于user类。
export const fileSchema = SchemaFactory.createForClass(file);