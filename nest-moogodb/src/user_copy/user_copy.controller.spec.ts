import { Test, TestingModule } from '@nestjs/testing';
import { UserCopyController } from './user_copy.controller';

describe('UserCopyController', () => {
  let controller: UserCopyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserCopyController],
    }).compile();

    controller = module.get<UserCopyController>(UserCopyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
