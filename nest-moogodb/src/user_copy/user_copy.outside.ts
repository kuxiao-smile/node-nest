import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { dateFormat } from '../utils'
// 表文件
export type CatDocument = user & Document;

@Schema()
// 定义了一个名为user的类，该类继承自Document类，表示该类的实例可以被存储到数据库中。
export class user extends Document {

  // @Prop()装饰器可以接受一些参数，例如default，用于指定属性的默认值。
  @Prop()
  user_name: string; // 账号

  @Prop()
  password: string; // 密码

  @Prop({default: ''}) // 年龄 默认 ''
  age: string;

  @Prop({default: ''}) // 性别 默认 ''
  sex: string;

  // default 如果不传则添加默认值
  @Prop({default: dateFormat('data')}) // 创建时间 默认当前时间
  date: string;
}

// 创建了一个名为userSchema的模式，该模式基于user类。
export const userSchema = SchemaFactory.createForClass(user);