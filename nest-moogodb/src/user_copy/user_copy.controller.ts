import { Controller, Get, Query, Post, Body } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ApiTags, ApiQuery, ApiCreatedResponse } from '@nestjs/swagger';
import { userResponses, userResponsesValidator, GetRequestDto } from './user_copy.swagger.validator'
import { GlobalParamsService } from '../utils/index'
// 生成token https://docs.nestjs.cn/9/security?id=jwt-%e5%8a%9f%e8%83%bd
import { JwtService } from '@nestjs/jwt';
//  添加特定标签 以下接口成为一组接口 -- 如果写到这里  则将会UserCopyController模块内的所有接口都加入到一个标签内
//  如果想将某几个接口放到一个标签内  则在接口上方写即可 -- 下面有示例
//  @ApiTags('Swagger文档')

@Controller('user-copy')
export class UserCopyController {
  constructor(
    // 依赖注入表文件，别名userModel 此表已挂载到app.module.ts文件内
    @InjectModel('user') private readonly userModel,
    // 依赖注入全局参数方法
    private readonly globalParamsService: GlobalParamsService,
    private readonly jwtService: JwtService
  ) { }

  // Swagger标签
  @ApiTags('无Swagger文档')
  // 请求方式：Post， 请求路径：/user-copy/add
  @Post('add')
  // @Body() 装饰器
  async addData(@Body() body) {
    // create 插入一条数据，直接将接收到的body参数插入
    const data = await this.userModel.create(body)
    if (data) {
      return { code: 200, data: null, message: "操作成功" }
    }
  }

  // Swagger标签
  @ApiTags('无Swagger文档')
  // 请求方式：Get， 请求路径：/user-copy/find
  @Get('find')
  // @Query() 装饰器
  async findData(@Query() query) {
    // find  查询指定数据
    const data = await this.userModel.find({ user_name: query.user_name })
    // 模糊查询
    // $regex 是 MongoDB 查询操作符，用于指定正则表达式进行匹配。$options: 'i' 表示不区分大小写，即忽略关键字的大小写。
    // const data = await this.userModel.find({ user_name: { $regex: query.user_name, $options: 'i' } })

    return { code: 200, data: data, message: "操作成功" }
  }

  // ----------------------- 以下示例接口将加入Swagger文档 -----------------------

  // Swagger标签
  @ApiTags('有Swagger文档')
  // 请求方式：Post， 请求路径：/user-copy/addCopy
  @Post('addCopy')
  // @Body() 装饰器
  async addDataCopy(@Body() body: userResponses) {
    // create 插入一条数据，直接将接收到的body参数插入
    const data = await this.userModel.create(body)
    if (data) {
      return { code: 200, data: null, message: "操作成功" }
    }
  }

  // Swagger标签
  @ApiTags('有Swagger文档')
  // 针对Get请求 使其可以在接口文档模拟传参
  @ApiQuery({ name: 'user_name' })
  // 请求方式：Get， 请求路径：/user-copy/findCopy
  @Get('findCopy')
  // 响应参数文档
  @ApiCreatedResponse({ description: '', type: userResponses })
  // @Query() 装饰器
  async findDataCopy(@Query() query) {
    // find  查询指定数据
    const data = await this.userModel.find({ user_name: query.user_name })
    // 模糊查询
    // $regex 是 MongoDB 查询操作符，用于指定正则表达式进行匹配。$options: 'i' 表示不区分大小写，即忽略关键字的大小写。
    // const data = await this.userModel.find({ user_name: { $regex: query.user_name, $options: 'i' } })

    return { code: 200, data: data, message: "操作成功" }
  }

  // ----------------------- 以下示例接口将加入Swagger文档，开启请求参数必填校验-----------------------

  // Swagger标签
  @ApiTags('有Swagger文档/开启请求参数校验')
  // 请求方式：Post， 请求路径：/user-copy/addValidator
  @Post('addValidator')
  // @Body() 装饰器
  async addDataValidator(@Body() body: userResponsesValidator) {
    // create 插入一条数据，直接将接收到的body参数插入
    const data = await this.userModel.create(body)
    if (data) {
      return { code: 200, data: null, message: "操作成功" }
    }
  }

  // Swagger标签
  @ApiTags('有Swagger文档/开启请求参数校验')
  // 针对Get请求 使其可以在接口文档模拟传参
  @ApiQuery({ name: 'user_name' })
  // 请求方式：Get， 请求路径：/user-copy/findValidator
  @Get('findValidator')
  // 响应参数文档
  @ApiCreatedResponse({ description: '', type: userResponses })
  // @Query() 装饰器
  async findDataValidator(@Query() query: GetRequestDto) {
    // find  查询指定数据
    const data = await this.userModel.find({ user_name: query.user_name })
    // 模糊查询
    // $regex 是 MongoDB 查询操作符，用于指定正则表达式进行匹配。$options: 'i' 表示不区分大小写，即忽略关键字的大小写。
    // const data = await this.userModel.find({ user_name: { $regex: query.user_name, $options: 'i' } })

    return { code: 200, data: data, message: "操作成功" }
  }

  // ----------------------- 以下示例接口将加入Swagger文档，开启请求参数必填校验-----------------------
  // ----------------------- 以下示例将返回 token，校验token, 并将接收的参数全局存储 { this.globalParamsService.setParam, this.globalParamsService.getParam } ------------------

  // Swagger标签
  @ApiTags('有Swagger文档/开启请求参数校验/Token')
  // 请求方式：Post， 请求路径：/user-copy/queryToken
  @Post('queryToken')
  // @Body() 装饰器
  async queryToken(@Body() body: userResponsesValidator) {
    // 存储参数到全局
    this.globalParamsService.setParam('globalToken', body);
    return {
      // 对参数进行签名  生成token -- 正常来说肯定是要去库里查这个账号存不存在的 -- 由于是示例代码  就不写那么麻烦了 -- 不然忘了账号又得再注册一个
      access_token: this.jwtService.sign({ user_name: body.user_name, password: body.password }),
    };
  }

  // Swagger标签
  @ApiTags('有Swagger文档/开启请求参数校验/Token')
  // 针对Get请求 使其可以在接口文档模拟传参
  @ApiQuery({ name: 'user_name' })
  // 请求方式：Get， 请求路径：/user-copy/findValidatorToken
  @Get('findValidatorToken')
  // 响应参数文档
  @ApiCreatedResponse({ description: '', type: userResponses })
  // @Query() 装饰器
  async findDataValidatorToken(@Query() query: GetRequestDto) {
    // 获取上面接口存储的值
    const obj = this.globalParamsService.getParam('globalToken');
    // find  查询指定数据
    const data = await this.userModel.find({ user_name: query.user_name })
    // 模糊查询
    // $regex 是 MongoDB 查询操作符，用于指定正则表达式进行匹配。$options: 'i' 表示不区分大小写，即忽略关键字的大小写。
    // const data = await this.userModel.find({ user_name: { $regex: query.user_name, $options: 'i' } })

    return { code: 200, data: data, message: "操作成功" }
  }
}


