// Swagger 文档配置 https://docs.nestjs.cn/8/recipes?id=swagger
import { ApiProperty } from '@nestjs/swagger';
// 请求参数验证（自动验证） https://docs.nestjs.cn/8/techniques?id=%e8%87%aa%e5%8a%a8%e9%aa%8c%e8%af%81
import { IsNotEmpty } from 'class-validator'

export class userResponses {
  // 接口文档展示信息 description 示例值  example 字段描述  required：true 则展示为必填
  @ApiProperty({ description: '', example: '账号', required: false })
  user_name: string;

  @ApiProperty({ description: '', example: '密码', required: false })
  password: string;

  @ApiProperty({ description: '', example: '年龄', required: false })
  age: string;

  @ApiProperty({ description: '', example: '性别', required: false })
  sex: string;

  @ApiProperty({ description: '', example: '创建时间', required: false })
  date: string;
}

export class userResponsesValidator {
  // 接口文档展示信息 description 示例值  example 字段描述  required：true 则展示为必填
  @ApiProperty({ description: '', example: '账号', required: true })
  @IsNotEmpty({message:'账号必填'})
  user_name: string;

  @ApiProperty({ description: '', example: '密码', required: true })
  @IsNotEmpty({message:'密码必填'})
  password: string;

  @ApiProperty({ description: '', example: '年龄', required: false })
  age: string;

  @ApiProperty({ description: '', example: '性别', required: false })
  sex: string;

  @ApiProperty({ description: '', example: '时间', required: false })
  date: string;
}

export class GetRequestDto {
  @ApiProperty({ description: '', example: '账号', required: true })
  @IsNotEmpty({message:'账号必填'})
  user_name: string;
}

export class queryTokenBody {
  @ApiProperty({ description: '', example: '账号', required: true })
  @IsNotEmpty({message:'账号必填'})
  user_name: string;

  @ApiProperty({ description: '', example: '密码', required: true })
  @IsNotEmpty({message:'密码必填'})
  password: string;
}