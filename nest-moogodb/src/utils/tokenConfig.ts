import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
// npm i jsonwebtoken
import * as jwt from 'jsonwebtoken';


interface JwtPayload {
  exp: number;
  // 其他 JWT 载荷属性
}

// 定义token的签名与超时时间
export const toeknData = {
  secret: "tokenKey", // 密匙
  expiresIn:"60s", // 超时时间60秒
}

@Injectable()
export class JwtExpiredMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {

    // 一般来说 可能会以正则去匹配白名单接口 - 大部分情况下,只会有极个别的接口会是白名单 - 由于是示例 就直接写死了
    // 如果 请求地址不为 /user-copy/findValidatorToken 则直接跳过
    if (req.baseUrl !== '/user-copy/findValidatorToken') return next()

    const token = req.headers.authorization?.split(' ')[1]; // 从请求头中获取 JWT

    if (token) {
      try {
        // 解码
        const decoded = jwt.verify(token, toeknData.secret) as JwtPayload;
        // 接收到JWT后执行的逻辑
        // 当前时间戳
        // const currentTime = Math.floor(Date.now() / 1000);
        // decoded.exp 是解码后的 JWT 中的过期时间，表示该令牌的有效期截止时间。
        // decoded.iat 是解码后的 JWT 中的签发时间，表示该令牌的生成时间。
        // 比较当前时间是否已经超出令牌有效期截至时间
        // if (decoded.exp < currentTime) {
        //   // JWT 已经超时
        //   // 执行相应的逻辑
        //   return res.send({ status: 99998, data: [], message: '登录超时' });
        // }
      } catch (error) {
        // JWT 验证失败
        // 执行相应的逻辑
        return res.send({ status: 99998, data: error, message: '认证失败' });
      }
    } else {
      // token认证失败
      // 执行相应的逻辑
      return res.send({ status: 99999, data: 'token in required', message: '认证失败' });
    }

    next();
  }

}