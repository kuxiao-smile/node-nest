// 处理时间
export const dateFormat = (type) => {
  const dt = new Date()

  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')
  const d = (dt.getDate() + '').padStart(2, '0')

  const hh = (dt.getHours() + '').padStart(2, '0')
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  const ss = (dt.getSeconds() + '').padStart(2, '0')
  
  if (type === 'year') {
    return `${y}`
  } else if (type === 'month') {
    return `${y}-${m}`
  } else if (type === 'data') {
    return `${y}-${m}-${d}`
  } else {
    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
  }
}


import { Injectable } from '@nestjs/common';
// 全局存储数据方法
@Injectable()
export class GlobalParamsService {
  private globalParams: any = {};

  setParam(key: string, value: any) {
    this.globalParams[key] = value;
  }

  getParam(key: string) {
    return this.globalParams[key];
  }
}