import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
// 管道  https://docs.nestjs.cn/8/techniques?id=%e8%87%aa%e5%8a%a8%e9%aa%8c%e8%af%81
// npm i --save class-validator class-transformer
import { ValidationPipe } from '@nestjs/common';

// Swagger 文档  https://docs.nestjs.cn/9/recipes?id=swagger
// npm install --save @nestjs/swagger swagger-ui-express
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // 全局挂载管道
  app.useGlobalPipes(new ValidationPipe());

  // Swagger 文档配置
  const options = new DocumentBuilder()
    .setTitle('Cats example') // 标题
    .setDescription('The cats API description') // 描述
    .setVersion('1.0') // 版本号
    .addTag('cats') // 标签
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document); // 接口文档访问路径 127.0.0.1:32000/api

  await app.listen(32000);
}
bootstrap();
