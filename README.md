# node-nest

#### 介绍
基于nest写的node项目（仅供学习），里面有moogodb连接与MySQL连接数据库两种方式，其中nest-moogodb为node项目，项目都有说明文档，建议仔细阅读。


- **这份代码注释全面，基本上是从0到1在写，毕竟容易理解**
- ![示例](./示例.png)


#### 软件架构
软件架构说明


#### 安装教程

1. 安装moogodb数据库: https://blog.csdn.net/muguli2008/article/details/80591256 
2. nest.js中文官网：https://docs.nestjs.cn/9/introduction
3. nest.js脚手架 / 新建项目：https://docs.nestjs.cn/9/introduction?id=%e5%ae%89%e8%a3%85

#### 使用说明

1.  nest-moogodb 为node服务
2.  nest-moogodb 启动命令 npm run start:dev


#### 参与贡献



